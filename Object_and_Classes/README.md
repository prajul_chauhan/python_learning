```This script is an example of how Classes, Objects & methods (functions within class) works in Python.```

***We are creating a class called User.
Using this class, We are providing the name, email, current_job_title & password for a user.
Later, we are also learning how to change password & current_job_title for that user.
If this user posts a message, it will also show the message along with username.***

```OUTPUT```

User Prajul Chauhan currently works as DevOps Engineer. You can contact them at prajul.chauhan2000@gmail.com

Post: learning Python written by Prajul Chauhan
