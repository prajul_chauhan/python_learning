```
Here, we learn how to work with excel/spreadsheets using Python.

"Inventory.xlsx" is a dummy inventory file that we are using.
```

## TASKS:

1) List each company with respective Product count.

2) List inventory with products less than 10.

3) List each company with respective total inventory value.

4) Calculate & write Inventory value for each product into spreadsheet in a new coloumn & save the sheet using Python.


### Note: Inventory.xlsx is to be used for practice while Final_Inventory.xlsx is the solution.
