from datetime import datetime

user_input = input("Enter your goal with deadline. Keep it separated with colon\n")
input_list = user_input.split(":")

goal = input_list[0]
deadline = input_list[1]

deadline_date = datetime.strptime(deadline, "%d.%m.%Y")
today_date = datetime.today()
final_deadline = deadline_date - today_date
print(f"Dear User! You have {final_deadline.days} days to {goal}")
