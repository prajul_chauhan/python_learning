##This script is used to convert number of days to Hours & Minutes##

```
The user has to input the number of days & the conversion unit.
```

```Example 1```

40:hours

40 days are 960 hours

```Example 2```

10:minutes

10 days are 14400 minutes
